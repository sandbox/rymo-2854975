# Writable Config

Core Drupal resets `settings.php` (and the folder containing it) to read-only
whenever you visit the System Status page.

This module counters that behavior by forcing the settings file and its
containing folder to writable.

## Why would you want this?
While developing a new site it can be frustrating to find this file constantly
and "mysteriously" reverting back to read-only as you're working to set up new
modules and perfect the configuration.

## Why wouldn't you want this?
Do *NOT* run this module in a production (or otherwise publicly accessible)
environment, that would be a security risk.

Consider using [Habitat](https://drupal.org/project/habitat) to keep this
module disabled in production and/or to keep it enabled in your local environment.
